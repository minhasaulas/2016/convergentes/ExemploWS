package br.edu.up;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/ws")
public class JaxRS {
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/getTexto")
	public String getTexto(){
		return "Ol� JAX-RS";
	}
}